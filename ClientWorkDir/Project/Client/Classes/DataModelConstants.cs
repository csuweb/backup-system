﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilemapBackupSystem.Classes
{
    public struct Chunk
    {
        public Int32 Id;
        public string Hash;
        public string Type;

        public Int64 Start;
        public Int64 End;
        public Int64 Length;
        public void CopyFrom(Chunk in1)
        {
            this.Id = in1.Id;
            this.Hash = in1.Hash;
            this.Type = in1.Type;

            this.Start = in1.Start;
            this.End = in1.End;
            this.Length = in1.Length;
        }
    }

    public struct XorUnitAttributes
    {
        public bool IsEqual;
        public Int64 DifferenceOffset;
    }

    public struct FileAttributes
    {
        public bool IsInited;
        public string FilemapName;
        public string Filename;
        public Int64 FileSize;
        public List<Chunk> chunks;
        public void CopyFrom(FileAttributes in1)
        {
            if(in1.FilemapName != null) this.FilemapName = in1.FilemapName;
            this.Filename = in1.Filename;
            this.FileSize = in1.FileSize;
            this.chunks = new List<Chunk>(in1.chunks.Count);
            foreach (var chunk in in1.chunks)
                this.chunks.Add(chunk);
        }

        public Chunk GetChunkById(int id)
        {
            Chunk result = new Chunk();
            for (int i = 0; i < chunks.Count; i++)
            {
                if (chunks[i].Id == id)
                    result.CopyFrom(chunks[i]);
            }
            return result;
        }

        public void Destroy()
        {
            this.IsInited = false;
            this.chunks.Clear();
            this.Filename = null;
            this.FileSize = 0;
        }
    }

    public static class ChunkType
    {
        public const string Static = "Static";
        public const string Variable = "Variable";
        public const string Extra = "Extra";
        public const string DifLen = "DifLen";
        public const string Undef = "Undef";
    }

    public static class FilemapTypes
    {
        public const string localFilemap = "localFilemap";
        public const string receivedFilemap = "receivedFilemap";
        public const string copyFilemap = "copyFilemap";
        public const string xorFilemap = "xorFilemap";
    }

    public static class DataModelConstants
    {
        public const Int64 chunkSize = 5242880; //5 mb
        public const Int64 xorUnitSize = 2097152; //2mb, меньше chunkSize

        public const string chunksDirectoryName = "chunks";
    }
}
