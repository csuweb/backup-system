﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilemapBackupSystem.Classes
{
    class Controller
    {
        private DataModelEditables dataModelEditables;

        public Controller()
        {
            this.dataModelEditables = new DataModelEditables();
        }

        // с сервера переданы чанки файла в ChunksTempDir, локальный файл
        // отсутствует
        // 0. считать полученный от сервера copyFilemap
        // 1. слить эти чанки в правильной последовательности
        // 2. поделить полученный файл на чанки, посчитать хэши чанков
        // 3. сохранить карту как xorFilemap
        public void ClientWork()
        {
            
            dataModelEditables.client.SetPath(Path.Combine(dataModelEditables.rootDir, dataModelEditables.filesDirName));
            LoadFilemap(FilemapTypes.xorFilemap);

            if (!dataModelEditables.FileWorker.IsFileExists(dataModelEditables.LocalFile) &&
                !dataModelEditables.XorFilemap.IsInited)
            {
                // если локальный файл и xorFilemap не были обнаружены - 
                // создать и сохранить пустой xorFilemap
                SaveFilemap(FilemapTypes.xorFilemap);
            }
            else
            {
                dataModelEditables.FileWorker.SplitFileToChunks(ref dataModelEditables.LocalFile, DataModelConstants.chunkSize, false);
            }

            // отправить xorFilemap на сервер
            dataModelEditables.client.Init();
            dataModelEditables.client.SendFile(new FileInfo(dataModelEditables.XorFilemap.FilemapName));

            // получить copyFilemap от сервера
            dataModelEditables.client.Init();
            dataModelEditables.client.ReceiveFile();
            LoadFilemap(FilemapTypes.copyFilemap);

            // получить чанки во временную папку
            dataModelEditables.client.SetPath(dataModelEditables.ChunksTempDirectoryName);
            for (int i = 0; i < dataModelEditables.CopyFilemap.chunks.Count; i++)
            {
                dataModelEditables.client.Init();
                dataModelEditables.client.ReceiveFile();
            }

            // поксорить локальный файл с загруженными чанками
            if (dataModelEditables.FileWorker.IsFileExists(dataModelEditables.LocalFile) &&
                dataModelEditables.XorFilemap.IsInited && dataModelEditables.CopyFilemap.IsInited)
            {
                // если локальный файл и xorFilemap не были обнаружены - 
                // создать и сохранить пустой xorFilemap
                dataModelEditables.FileWorker.XorFileToChunks(dataModelEditables.LocalFile,
                                                              dataModelEditables.ChunksTempDirectoryName,
                                                              ref dataModelEditables.XorFilemap,
                                                              dataModelEditables.CopyFilemap,
                                                              DataModelConstants.xorUnitSize,
                                                              dataModelEditables.ReadWriteBuffer);
            }

            if (dataModelEditables.CopyFilemap.IsInited)
            {
                dataModelEditables.FileWorker.MergeChunks(dataModelEditables.LocalFile, dataModelEditables.CopyFilemap,
                                                          ref dataModelEditables.MergedFile,
                                                          dataModelEditables.ChunksTempDirectoryName,
                                                          dataModelEditables.ReadWriteBuffer);
            }

            
            if (!dataModelEditables.XorFilemap.IsInited)
            {
                dataModelEditables.FileWorker.SplitFileToChunks(ref dataModelEditables.LocalFile, DataModelConstants.chunkSize, false);

                dataModelEditables.FileWorker.CreateDefaultXorFilemap(dataModelEditables.LocalFile,
                    ref dataModelEditables.XorFilemap, DataModelConstants.chunkSize);
            }
            SaveFilemap(FilemapTypes.xorFilemap);
        }

        // 1. на сервер отправляем карту копирования. 
        //      *если она пустая - делим весь файл на чанки, формируем из этого 
        // copyFilemap(без хэшей), передаем
        // карту и все чанки клиенту
        //      *если не пустая - считываем ее, делим локальный файл на части
        // среди карты ксор ищем схожие области. если схожая область из xorFilemap:
        //          * статичная - читаем эту область из файла, считаем ее хэш, сравниваем с
        //          хэшем из xorFilemap. если не совпали - добавляем в copyFilemap
        //          * иные виды совпавших областей добавляем в copyFilemap
        // если схожей области не нашлось - добавляем ее в copyFilemap как Extra
        // 2. сохраняем xorFilemap
        // 3. извлечь области, вошедшие в copyFilemap, в ChunksTempDir
        public void SaveFilemap(string filemapType)
        {
            switch (filemapType)
            {
                case FilemapTypes.copyFilemap:
                    dataModelEditables.FilemapWorker.SaveFilemap(dataModelEditables.CopyFilemap);
                    break;
                case FilemapTypes.xorFilemap:
                    dataModelEditables.FilemapWorker.SaveFilemap(dataModelEditables.XorFilemap);
                    break;
            }
        }

        public void LoadFilemap(string filemapType)
        {
            switch (filemapType)
            {
                case FilemapTypes.copyFilemap:
                    dataModelEditables.FilemapWorker.LoadFilemap(ref dataModelEditables.CopyFilemap);
                    break;
                case FilemapTypes.xorFilemap:
                    dataModelEditables.FilemapWorker.LoadFilemap(ref dataModelEditables.XorFilemap);
                    break;
            }
        }

        //// +---------------------------------------------------+
        //#region Split

        //// разбить локальный хранимый файл на чанки
        //public void SplitLocalFile()
        //{
        //    dataModelEditables.FileWorker.SplitFileToChunks(ref dataModelEditables.LocalFile, DataModelConstants.chunkSize);
        //}

        //// разбить принятый файл на чанки
        //public void SplitReceivedFile()
        //{
        //    dataModelEditables.FileWorker.SplitFileToChunks(ref dataModelEditables.ReceivedFile, DataModelConstants.chunkSize);
        //}

        //#endregion Split
        //// +---------------------------------------------------+

        //// +---------------------------------------------------+
        //#region Xor

        //// сравнить принятый и локальный файлы операцией xor
        //// и сформировать карту чанков xor-filemap без хэшей
        //// на основе сравнения
        //public void XorLocalAndReceivedFiles()
        //{
        //    dataModelEditables.FileWorker.XorFiles(dataModelEditables.LocalFile,
        //                                           dataModelEditables.ReceivedFile,
        //                                           ref dataModelEditables.XorFilemap,
        //                                           DataModelConstants.xorUnitSize);
        //}

        //// добавить хэши к xor-карте
        //public void GenerateXorFilemapHashes()
        //{
        //    dataModelEditables.FileWorker.GenerateFilesChunksHash(ref dataModelEditables.XorFilemap, dataModelEditables.LocalFile);
        //}
        //#endregion Xor
        //// +---------------------------------------------------+
        //#region Split/Merge
        //public void ExtractChunks()
        //{
        //    dataModelEditables.FileWorker.ExtractChunks(dataModelEditables.ReceivedFile, dataModelEditables.CopyFilemap,
        //                                                dataModelEditables.ChunksTempDirectoryName,
        //                                                dataModelEditables.ReadWriteBuffer);
        //}

        //public void MergeChunks()
        //{
        //    dataModelEditables.FileWorker.MergeChunks(dataModelEditables.LocalFile, dataModelEditables.CopyFilemap,
        //                                              ref dataModelEditables.MergedFile,
        //                                              dataModelEditables.ChunksTempDirectoryName,
        //                                              dataModelEditables.ReadWriteBuffer, DataModelConstants.chunkSize, DataModelConstants.xorUnitSize);
        //}
        //#endregion Split/Merge
        //// +---------------------------------------------------+

        //// сформировать карту копирования на основе карты операции xor
        //// и локально хранимого файла
        //public void GenerateCopyFilemap()
        //{
        //    dataModelEditables.FileWorker.GenerateCopyFilemap(dataModelEditables.LocalFile,
        //                                                      dataModelEditables.XorFilemap,
        //                                                      ref dataModelEditables.CopyFilemap,
        //                                                      DataModelConstants.chunkSize);
        //}

    }
}
