﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FilemapBackupSystem.Classes
{
    public class Server
    {
        public TcpListener listener { get; set; }
        public TcpClient client { get; set; }
        public NetworkStream stream { get; set; }
        public string filesDir;

        #region Receive

        public Server()
        {
            listener = new TcpListener(IPAddress.Parse("127.0.0.1"), 1488);
            //listener.Start();
            //new Thread(ListenForClients).Start();
        }

        public void SetPath(string filesDir)
        {
            this.filesDir = filesDir;
        }

        public void WaitClient()
        {
            listener.Start();
            Console.WriteLine("Сервер поднят на " +
                              IPAddress.Parse(((IPEndPoint)listener.LocalEndpoint).Address.ToString()) + " : " +
                              ((IPEndPoint)listener.LocalEndpoint).Port.ToString());
            TcpClient client = listener.AcceptTcpClient();
            this.client = client;
        }

        //private void ListenForClients()
        //{
        //    while (true)
        //    {
        //        TcpClient client = this.listener.AcceptTcpClient();
        //        this.client = client;
        //        new Thread(HandleClient).Start(client);
        //    }
        //}

        //public void HandleClient(object tcpClient)
        //{
        //    TcpClient client = (TcpClient)tcpClient;
        //    ReceiveFile(client);
        //    //while (client.Connected)
        //    //{

        //    //}
        //}

        //private void HandleClient(object tcpClient)
        //{
        //    TcpClient client = (TcpClient)tcpClient;
        //    while (client.Connected)
        //    {
        //        ReceiveFile(client);
        //    }
        //}

        public void ReceiveFile()
        {
            byte[] buffer = new byte[client.ReceiveBufferSize];
            try
            {
                int bytesRead = stream.Read(buffer, 0, 12);
                if (bytesRead == 0) return;

                ushort id = BitConverter.ToUInt16(buffer, 0);
                long len = BitConverter.ToInt64(buffer, 2);
                ushort nameLen = BitConverter.ToUInt16(buffer, 10);
                stream.Read(buffer, 0, nameLen);
                string fileName = Encoding.UTF8.GetString(buffer, 0, nameLen);

                if (id == 1)
                {
                    using (BinaryWriter writer = new BinaryWriter(new FileStream(Path.Combine(filesDir, fileName), FileMode.Create)))
                    {
                        int recieved = 0;
                        while (recieved < len)
                        {
                            bytesRead = stream.Read(buffer, 0, client.ReceiveBufferSize);
                            recieved += bytesRead;
                            writer.Write(buffer, 0, bytesRead);
                            //Console.WriteLine("{0} bytes recieved.", recieved);
                        }
                    }
                    Console.WriteLine("File length: {0}", len);
                    Console.WriteLine("File Name: {0}", fileName);
                    Console.WriteLine("Recieved!");
                }
                else
                {
                    Console.WriteLine(fileName);
                }
            }
            catch (Exception)
            {
                stream.Close();
                client.Close();
            }
            finally
            {
                stream.Flush();
            }
        }
        #endregion Receive

        #region Send

        //public void SendFile(FileInfo file)
        //{
        //    byte[] id = BitConverter.GetBytes((ushort) 1);
        //    byte[] size = BitConverter.GetBytes(file.Length);
        //    byte[] fileName = Encoding.UTF8.GetBytes(file.Name);
        //    byte[] fileNameLength = BitConverter.GetBytes((ushort) fileName.Length);
        //    byte[] fileInfo = new byte[12 + fileName.Length];

        //    id.CopyTo(fileInfo, 0);
        //    size.CopyTo(fileInfo, 2);
        //    fileNameLength.CopyTo(fileInfo, 10);
        //    fileName.CopyTo(fileInfo, 12);

        //    stream.Write(fileInfo, 0, fileInfo.Length); //Размер файла, имя

        //    byte[] buffer = new byte[1024*32];
        //    int count;

        //    long sended = 0;

        //    using (FileStream fileStream = new FileStream(file.FullName, FileMode.Open))
        //        while ((count = fileStream.Read(buffer, 0, buffer.Length)) > 0)
        //        {
        //            stream.Write(buffer, 0, count);
        //            sended += count;
        //            Console.WriteLine("{0} bytes sended.", sended);
        //        }
        //    stream.Flush();
        //}

        //public void SendMessage(string message)
        //{
        //    byte[] id = BitConverter.GetBytes((ushort)0);
        //    byte[] msg = Encoding.UTF8.GetBytes(message);
        //    byte[] msgLength = BitConverter.GetBytes((ushort)msg.Length);
        //    byte[] fileInfo = new byte[12 + msg.Length];

        //    id.CopyTo(fileInfo, 0);
        //    msgLength.CopyTo(fileInfo, 10);
        //    msg.CopyTo(fileInfo, 12);

        //    stream.Write(fileInfo, 0, fileInfo.Length);
        //    stream.Flush();
        //}

        #endregion Send
    }
}
