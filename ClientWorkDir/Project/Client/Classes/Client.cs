﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace FilemapBackupSystem.Classes
{
    public class Client : Server
    {
        public TcpClient client;
        public NetworkStream stream;
        public string filesDir;

        public void Init()
        {
            string temp;
            client = new TcpClient("localhost", 1488);
            client.ReceiveTimeout = 0;
            client.SendTimeout = 0;

            GC.KeepAlive(client);
            stream = client.GetStream();
            GC.KeepAlive(stream);

            Console.WriteLine(client.SendBufferSize);

            //while ((temp = Console.ReadLine()) != "exit")
            //{
            //    Send(temp);
            //}

            //SendFile(new FileInfo(@"File_path"));
            //Console.ReadKey(true);
        }

        public void SetPath(string filesDir)
        {
            this.filesDir = filesDir;
        }

        public void SendFile(FileInfo file)
        {
            byte[] id = BitConverter.GetBytes((ushort)1);
            byte[] size = BitConverter.GetBytes(file.Length);
            byte[] fileName = Encoding.UTF8.GetBytes(file.Name);
            byte[] fileNameLength = BitConverter.GetBytes((ushort)fileName.Length);
            byte[] fileInfo = new byte[12 + fileName.Length];

            id.CopyTo(fileInfo, 0);
            size.CopyTo(fileInfo, 2);
            fileNameLength.CopyTo(fileInfo, 10);
            fileName.CopyTo(fileInfo, 12);

            stream.Write(fileInfo, 0, fileInfo.Length); //Размер файла, имя

            byte[] buffer = new byte[1024 * 32];
            int count;

            long sended = 0;

            using (FileStream fileStream = new FileStream(file.FullName, FileMode.Open))
                while ((count = fileStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    stream.Write(buffer, 0, count);
                    sended += count;
                    //Console.WriteLine("{0} bytes sended.", sended);
                }
            Console.WriteLine("File " + file.Name + " sended: "+ sended + " bytes");
            stream.Flush();
            client.Close();
        }


        public void SendMessage(string message)
        {
            byte[] id = BitConverter.GetBytes((ushort)0);
            byte[] msg = Encoding.UTF8.GetBytes(message);
            byte[] msgLength = BitConverter.GetBytes((ushort)msg.Length);
            byte[] fileInfo = new byte[12 + msg.Length];

            id.CopyTo(fileInfo, 0);
            msgLength.CopyTo(fileInfo, 10);
            msg.CopyTo(fileInfo, 12);

            stream.Write(fileInfo, 0, fileInfo.Length);
            stream.Flush();
            client.Close();
        }

        public void ReceiveFile()
        {
            base.stream = client.GetStream();
            base.client = client;
            base.filesDir = filesDir;
            base.ReceiveFile();
            client.Close();
        }
    }
}
