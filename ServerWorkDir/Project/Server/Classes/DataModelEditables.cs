﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FilemapBackupSystem.Classes
{
    class DataModelEditables
    {
        public string dir = @"02082015";

        public string rootDir = @"C:\Users\Parabellum\Dropbox\Work\Система резервного копирования облака\serverWorkDir";
        public string filesDirName = @"Files";

        public int ReadWriteBuffer = 10485760; // 10mb


        public string LocalFileName
        {
            get { return Path.Combine(rootDir, filesDirName, "localFile.exe"); }
        }

        public string MergedFileName
        {
            get { return Path.Combine(rootDir, filesDirName, "merged.exe"); }
        }

        public string ReceivedFileName
        {
            get { return Path.Combine(rootDir, filesDirName, "receivedFile.exe"); }
        }

        public string ReceivedFileMapName
        {
            get { return Path.Combine(rootDir, filesDirName, "receivedFilemap.xml"); }
        }

        public string LocalFileMapName
        {
            get { return Path.Combine(rootDir, filesDirName, "localFilemap.xml"); }
        }

        public string XorFilemapName
        {
            get { return Path.Combine(rootDir, filesDirName, "xorFilemap.xml"); }
        }

        public string CopyFilemapName
        {
            get { return Path.Combine(rootDir, filesDirName, "copyFilemap.xml"); }
        }

        public string ChunksTempDirectoryName
        {
            get { return Path.Combine(rootDir, filesDirName, "chunksTempDir"); }
        }

        public FileAttributes LocalFile;
        public FileAttributes MergedFile;
        public FileAttributes ReceivedFile;
        public FileAttributes CopyFilemap;
        public FileAttributes XorFilemap;

        public Exceptioner Exceptioner;
        public FileWorker FileWorker;
        public Utils Utils;
        public FilemapWorker FilemapWorker;

        public Client client;
        public Server server;

        public DataModelEditables()
        {
            this.Exceptioner = new Exceptioner();
            this.FileWorker = new FileWorker();
            this.Utils = new Utils();
            this.FilemapWorker = new FilemapWorker();
            this.client = new Client();
            this.server = new Server();

            this.LocalFile = new FileAttributes { chunks = new List<Chunk>(), Filename = LocalFileName };
            this.MergedFile = new FileAttributes { chunks = new List<Chunk>(), Filename = MergedFileName };
            this.ReceivedFile = new FileAttributes { chunks = new List<Chunk>(), Filename = ReceivedFileName };

            this.CopyFilemap = new FileAttributes { chunks = new List<Chunk>(), FilemapName = CopyFilemapName };
            this.XorFilemap = new FileAttributes { chunks = new List<Chunk>(), FilemapName = XorFilemapName };
        }


    }
}
