﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using ExtensionMethods;
namespace FilemapBackupSystem.Classes
{
    class FileWorker
    {
        private Utils _utils;

        public FileWorker()
        {
           this._utils = new Utils();
        }

        public void CheckFileExists(FileAttributes file)
        {
            if (!File.Exists(file.Filename))
                throw new Exception(string.Format("Файл \"{0}\" недоступен", file.Filename));
        }

        public void CheckDirExists(string dir)
        {
            if (!Directory.Exists(dir))
                throw new Exception(string.Format("Директория \"{0}\" недоступна", dir));
        }

        // посчитать количество чанков в файл (округление до большего)
        private static Int32 CalculateNumOfChunksInFile(string fileName, Int64 chunkSize)
        {
            FileInfo fileInfo = new FileInfo(fileName);
            Decimal dLenght = fileInfo.Length;
            Decimal dChunkSize = chunkSize;
            return (Int32) Math.Ceiling(dLenght/dChunkSize);
        }

        // разбить файл на чанки, заполнив позиционную информацию и id (он же порядковый номер)
        public void SplitFileToChunks(ref FileAttributes file, Int64 chunkSize, bool toGenHash)
        {
            FileInfo fileInfo = new FileInfo(file.Filename);

            if (!File.Exists(file.Filename)) return;

            Int32 chunksInFile = CalculateNumOfChunksInFile(file.Filename, chunkSize);
            file.chunks = new List<Chunk>(chunksInFile);
            file.FileSize = fileInfo.Length;

            if(chunksInFile < 1)
                throw new Exception("В процессе подсчета количества чанков что-то пошло не так");

            for (int i = 0; i < chunksInFile; i++)
            {
                Chunk tempChunk = new Chunk
                    {
                        Id = i,
                        Start = i*chunkSize,
                        End = chunkSize*(i + 1) > fileInfo.Length ? fileInfo.Length : chunkSize*(i + 1),
                        Length =
                            chunkSize*(i + 1) > fileInfo.Length ? Math.Abs(fileInfo.Length - chunkSize*i) : chunkSize
                    };
                if (toGenHash)
                {
                    tempChunk.Hash = GenerateChunkHash(file.Filename, tempChunk, true);
                    tempChunk.Type = ChunkType.Static;
                }
                file.chunks.Add(tempChunk);
            }
        }

        // заполнить структуру чанков, заполненную позиционной информацией, хэшами на основании filename
        public void GenerateFilesChunksHash(ref FileAttributes filemap, FileAttributes file)
        {
            for (int i = 0; i < filemap.chunks.Count(); i++)
            {
                if(filemap.chunks[i].Type == null)
                    throw new Exception(string.Format("Чанк {0} имеет неопределенный тип", i));

                if (filemap.chunks[i].Type == ChunkType.Static ||
                    filemap.chunks[i].Type == ChunkType.Extra)
                {
                    Chunk tempChunk = filemap.chunks[i];
                    tempChunk.Hash = GenerateChunkHash(file.Filename, filemap.chunks[i], true);
                    filemap.chunks[i] = tempChunk;
                }
            }
        }

        // сгенерировать хэш массива байт
        private string GenerateByteArrayHash(HashAlgorithm hashAlg, byte[] data, int lenght)
        {
            byte[] hashData = hashAlg.ComputeHash(data, 0, lenght);
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < hashData.Count(); i++)
                stringBuilder.Append(hashData[i].ToString("x2"));
            return stringBuilder.ToString();
        }

        // считать из fileStream область размером chunk.Lenght и 
        // посчитать ее хэш
        private string GenerateChunkHash(string filename, Chunk chunk, bool toSeekFilestream)
        {
            MD5 hashAlg = MD5.Create();
            byte[] buffer = new byte[chunk.Length];
            FileStream fileStream = File.OpenRead(filename);

            if(toSeekFilestream)
                fileStream.Seek(chunk.Start, SeekOrigin.Begin);

            fileStream.Read(buffer, 0, (int)chunk.Length);
            fileStream.Close();

            return GenerateByteArrayHash(hashAlg, buffer, (int)chunk.Length);
        }


        public string XorChunks(Chunk localChunk, Chunk receivedChunk, string LocalFile, string ReceivedFile, Int64 xorUnitSize, bool toSeekReceivedFilestream)
        {
            string chunkType = null;

            FileStream fsLocalFile = File.OpenRead(LocalFile);
            FileStream fsReceivedFile = File.OpenRead(ReceivedFile);

            if(fsLocalFile.Length < localChunk.Start + localChunk.Length)
                throw new Exception("Неверные параметры локального чанка");

            if (fsReceivedFile.Length < receivedChunk.Length)
                throw new Exception("Неверные параметры принятого файла");

            //if (fsReceivedFile.Length < receivedChunk.Start + receivedChunk.Length)
            //    throw new Exception("Неверные параметры принятого файла");

            //if(fsLocalFile.Length != fsReceivedFile.Length)
            //    throw new Exception("Чанки разной длины");

            //if (fsLocalFile.Length < receivedChunk.Start + receivedChunk.Length)
            //{
            //    chunkType = ChunkType.Extra;
            //    return chunkType;
            //}

            fsLocalFile.Seek(localChunk.Start, SeekOrigin.Begin);

            if (toSeekReceivedFilestream)
                fsReceivedFile.Seek(receivedChunk.Start, SeekOrigin.Begin);

            //if (localChunk.Length == receivedChunk.Length)
            //{
            byte[] localFileBytes = new byte[xorUnitSize];
            byte[] receivedFileBytes = new byte[xorUnitSize];

            Int64 totalBytesReadLf = 0;
            Int64 totalBytesReadRf = 0;

            while (totalBytesReadLf < localChunk.Length)
            {
                Int32 bytesToRead = totalBytesReadLf + localFileBytes.Length > localChunk.Length
                                        ? (Int32)(localChunk.Length - totalBytesReadLf)
                                        : localFileBytes.Length;

                Int32 bytesReadLf = fsLocalFile.Read(localFileBytes, 0, bytesToRead);
                totalBytesReadLf += bytesReadLf;

                Int32 bytesReadRf = fsReceivedFile.Read(receivedFileBytes, 0, bytesToRead);
                totalBytesReadRf += bytesReadRf;

                if (totalBytesReadLf != totalBytesReadRf || bytesReadLf != bytesReadRf)
                    throw new Exception("Непредвиденный размер чанков");

                if (IsXorUnitsEqual(localFileBytes, receivedFileBytes, bytesReadLf).IsEqual != true)
                {
                    chunkType = ChunkType.Variable;
                    break;
                }
                chunkType = ChunkType.Static;
            }
            //}
            //else
            //{
            //    chunkType = ChunkType.DifLen;
            //}
            fsLocalFile.Close();
            fsReceivedFile.Close();
            return chunkType;
        }

        public bool IsAllFilesExists(string chunksTempDirectory, List<Chunk> chunks)
        {
            bool result = false;

            string[] filesInDir = Directory.GetFiles(chunksTempDirectory);

            foreach (Chunk tempChunk in chunks)
            {
                bool match = false;
                for (int j = 0; j < filesInDir.Count(); j++)
                {
                    Int32 int32ChunkId;
                    Int32.TryParse(Path.GetFileNameWithoutExtension(filesInDir[j]), out int32ChunkId);
                    if (tempChunk.Id != int32ChunkId) continue;
                    match = true;
                    break;
                }
                if (match == false)
                {
                    //throw new Exception(string.Format("Чанк {0} не обнаружен", tempChunk.Id));
                    result = false;
                    break;
                }
                result = true;
            }
            return result;
        }

        // операция xor для файла и чанков из chunksTempDirectory
        // результаты заносятся в xorFilemap
        public void XorFileToChunks(FileAttributes localFile, string chunksTempDirectory, ref FileAttributes xorFilemap,
                                    FileAttributes copyFilemap, Int64 xorUnitSize, int readWriteBuffer)
        {
            if(localFile.chunks.Count == 0)
                throw new Exception("Локальный файл не разбит на чанки");

            // проверка наличия всех необходимых чанков
            if(!IsAllFilesExists(chunksTempDirectory, copyFilemap.chunks))
                throw new Exception("Не все чанки были обнаружены!");

            // операция локальный файл ^ чанки, обнаруженные в copyFilemap
            for (int i = 0; i < localFile.chunks.Count; i++)
            {
                Chunk tempChunk = localFile.chunks[i];
                int sameChunkIndex = ChunkPosInChunksArray(localFile.chunks[i], copyFilemap.chunks);
                // если чанк локального файла существует в карте копирования - 
                // ксорим и меняем тип
                if (sameChunkIndex >= 0)
                {
                    tempChunk = copyFilemap.chunks[sameChunkIndex];
                    tempChunk.Type = XorChunks(localFile.chunks[i], tempChunk, localFile.Filename,
                        Path.Combine(chunksTempDirectory, tempChunk.Id.ToString()), xorUnitSize, false);
                }
                // если чанк есть в локальном файле, но его нет в карте копирования -
                // считаем, что чанк статичен
                else
                {
                    tempChunk.Type = ChunkType.Static;
                }
                if (tempChunk.Type == ChunkType.Extra || tempChunk.Type == ChunkType.Static)
                {
                    tempChunk.Hash = GenerateChunkHash(localFile.Filename, tempChunk, true);
                }
                else if (tempChunk.Type == ChunkType.Variable)
                {
                    tempChunk.Hash = null;
                }
                xorFilemap.chunks.Add(tempChunk);
            }
            // добавить все чанки, отсутствующие в локальном файле но принятые с сервера
            for (int i = 0; i < copyFilemap.chunks.Count; i++)
            {
                Chunk tempChunk = copyFilemap.chunks[i];
                int sameChunkIndex = ChunkPosInChunksArray(copyFilemap.chunks[i], localFile.chunks);
                if (sameChunkIndex < 0)
                {
                    tempChunk.Type = ChunkType.Extra;
                    xorFilemap.chunks.Add(tempChunk);
                }
            }
        }

        public int ChunkPosInChunksArray(Chunk target, List<Chunk> chunks)
        {
            int result = -1;
            for (int i = 0; i < chunks.Count; i++)
            {
                if (chunks[i].Start != target.Start || chunks[i].End != target.End || chunks[i].Length != target.Length)
                    continue;
                result = i;
                break;
            }
            return result;
        }

        // побайтовая операция исключающего или для массивов байт 
        private static XorUnitAttributes IsXorUnitsEqual(byte[] xorUnit1, byte[] xorUnit2, int bytesToXor)
        {
            XorUnitAttributes xorUnitAttributes;
            if(xorUnit1.Length != xorUnit2.Length)
                throw new Exception("КсорЮниты имеют разный размер");

            for (Int64 i = 0; i < bytesToXor; i++)
            {
                if ((byte)(xorUnit1[i] ^ xorUnit2[i]) != (byte)0)
                {
                    xorUnitAttributes.IsEqual = false;
                    xorUnitAttributes.DifferenceOffset = i;
                    return xorUnitAttributes;
                }
            }

            xorUnitAttributes.DifferenceOffset = 0;
            xorUnitAttributes.IsEqual = true;
            return xorUnitAttributes;
        }

        // сформировать карту копирования на основании уже сформированных карт локального файла и
        // карты xor
        public void GenerateCopyFilemap(FileAttributes localFile, FileAttributes xorFilemap,
                                        ref FileAttributes copyFilemap)
        {
            if (localFile.chunks.Count == 0)
                throw new Exception("Локальный файл не разбит на чанки");

            for (int i = 0; i < localFile.chunks.Count; i++)
            {
                Chunk tempChunk = localFile.chunks[i];
                int sameChunkIndex = ChunkPosInChunksArray(localFile.chunks[i], xorFilemap.chunks);
                if (sameChunkIndex >= 0)
                {
                    tempChunk = xorFilemap.chunks[sameChunkIndex];
                    if (tempChunk.Type == ChunkType.Static)
                    {
                        if (GenerateChunkHash(localFile.Filename, tempChunk, true) != tempChunk.Hash)
                        {
                            tempChunk.Type = ChunkType.Variable;
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
                else
                {
                    tempChunk.Type = ChunkType.Extra;
                }
                copyFilemap.chunks.Add(tempChunk);
            }
            copyFilemap.FileSize = localFile.FileSize;
            copyFilemap.Filename = localFile.Filename;
        }

        //for (int i = 0; i < xorFilemap.chunks.Count; i++)
            //{
            //    Chunk xorTempChunk = FindChunkByParameters()
            //    // находим такой же чанк в принятой карте файла
            //    for (int j = 0; j < localFile.chunks.Count; j++)
            //    {

            //        if (localFile.chunks[j].Start == xorFilemap.chunks[i].Start &&
            //            localFile.chunks[j].End == xorFilemap.chunks[i].End &&
            //            localFile.chunks[j].Length == xorFilemap.chunks[i].Length)
            //        {
            //            xorTempChunk = xorFilemap.chunks[i];
            //            break;
            //        }
            //    }
            //    // если такой чанк нашелся - обрабатываем его
            //    if (xorTempChunk.Type != null)
            //    {
            //        // если чанк статичный считываем часть файла, соотв этому чанку,
            //        // считаем ее хэш, если хэши не совпали - добавляем в карту копирования
            //        if (xorTempChunk.Type == ChunkType.Static)
            //        {
            //            Chunk localTempChunk = localFile.chunks[i];

            //            fileStream.Seek(localTempChunk.Start, SeekOrigin.Begin);
            //            int bytesRead = fileStream.Read(data, 0, (int) localTempChunk.Length);

            //            localTempChunk.Hash = GenerateByteArrayHash(md5, data, bytesRead);

            //            if (localTempChunk.Hash != xorTempChunk.Hash)
            //            {
            //                localTempChunk.Type = ChunkType.Variable;
            //                copyFilemap.chunks.Add(localTempChunk);
            //            }
            //        }
            //        // если чанк изменяемый - переносим его в карту копирования
            //        else
            //        {
            //            copyFilemap.chunks.Add(xorTempChunk);
            //        }
            //    }
            //    // если нет - добавляем как избыточный
            //    else
            //    {
            //        xorTempChunk = xorFilemap.chunks[i];
            //        copyFilemap.chunks.Add(xorTempChunk);
            //        //xorTempChunk.Type = ChunkType.Extra;
            //        //xorTempChunk.Type = ChunkType.Variable;
            //    }
            //}

        // извлечь чанки предназначенные для копирования
        public void ExtractChunks(FileAttributes localFile, FileAttributes copyFilemap, string chunkTempDirectory,
                                  int readWriteBuffer)
        {
            //FileStream localFileStream = File.OpenRead(localFile.Filename);
            //byte[] buffer = new byte[readWriteBuffer];
            for (int i = 0; i < copyFilemap.chunks.Count; i++)
            {
                if (
                    File.Exists(Path.Combine(chunkTempDirectory,
                        copyFilemap.chunks[i].Id.ToString(CultureInfo.InvariantCulture))))
                    throw new Exception("Обнаружены уже извлеченные файлы!");

                CopyUsingBuffer(localFile.Filename, copyFilemap.chunks[i].Start,
                    Path.Combine(chunkTempDirectory, copyFilemap.chunks[i].Id.ToString(CultureInfo.InvariantCulture)), 0,
                    copyFilemap.chunks[i].Length, readWriteBuffer);
            }
            //localFileStream.Seek(copyFilemap.chunks[i].Start, SeekOrigin.Begin);
                //using (FileStream chunkFileStream = File.Create(Path.Combine(chunkTempDirectory, copyFilemap.chunks[i].Id.ToString())))
                //{
                //    int totalBytesRead = 0;
                //    while (totalBytesRead < copyFilemap.chunks[i].Length)
                //    {
                //        Int32 bytesToRead = totalBytesRead + buffer.Length > copyFilemap.chunks[i].Length
                //                            ? (Int32)(copyFilemap.chunks[i].Length - totalBytesRead)
                //                            : buffer.Length;

                //        int bytesRead = localFileStream.Read(buffer, 0, bytesToRead);
                //        totalBytesRead += bytesRead;
                //        chunkFileStream.Write(buffer, 0, bytesRead);
                //    }
                //}
        }

        public Int64 PasteUsingBuffer(string srcFile, Int64 srcOffset, string dstFile, Int64 dstOffset, Int64 bytesToCopy, int readWriteBuffer)
        {
            FileInfo srcFileInfo = new FileInfo(srcFile);

            if (srcFileInfo.Length < bytesToCopy)
                throw new Exception("Кол-во запрошенных байт больше длины файла");

            Int64 totalBytesRead = 0;
            byte[] buffer = new byte[readWriteBuffer];

            using (FileStream srcFs = srcFileInfo.OpenRead())
            {
                using (FileStream dstFs = File.OpenWrite(dstFile))
                {
                    srcFs.Seek(srcOffset, SeekOrigin.Begin);
                    dstFs.Seek(dstOffset, SeekOrigin.Begin);
                    while (totalBytesRead < bytesToCopy && srcFs.Position != srcFs.Length)
                    {
                        Int32 bytesToRead = totalBytesRead + buffer.Length > bytesToCopy
                                                ? (Int32)(bytesToCopy - totalBytesRead)
                                                : buffer.Length;
                        int bytesRead = srcFs.Read(buffer, 0, bytesToRead);
                        dstFs.Write(buffer, 0, bytesRead);
                        totalBytesRead += bytesRead;
                    }
                    dstFs.Close();
                }
                srcFs.Close();
            }
            return totalBytesRead;
        }

        public Int64 CopyUsingBuffer(string srcFile, Int64 srcOffset, string dstFile, Int64 dstOffset, Int64 bytesToCopy, int readWriteBuffer)
        {
            FileInfo srcFileInfo = new FileInfo(srcFile);

            if (File.Exists(dstFile))
                throw new Exception("Файл назначения уже создан!");

            if (srcFileInfo.Length < bytesToCopy)
                throw new Exception("Кол-во запрошенных байт больше длины файла");

            Int64 totalBytesRead = 0;
            byte[] buffer = new byte[readWriteBuffer];

            using (FileStream srcFs = srcFileInfo.OpenRead())
            {
                using (FileStream dstFs = File.Create(dstFile))
                {
                    srcFs.Seek(srcOffset, SeekOrigin.Begin);
                    dstFs.Seek(dstOffset, SeekOrigin.Begin);
                    while (totalBytesRead < bytesToCopy && srcFs.Position != srcFs.Length)
                    {
                        Int32 bytesToRead = totalBytesRead + buffer.Length > bytesToCopy
                                                ? (Int32)(bytesToCopy - totalBytesRead)
                                                : buffer.Length;
                        int bytesRead = srcFs.Read(buffer, 0, bytesToRead);
                        dstFs.Write(buffer, 0, bytesRead);
                        totalBytesRead += bytesRead;
                    }
                }
                srcFs.Close();
            }
            return totalBytesRead;
        }

        public void UpdateXorFilemap(FileAttributes localFile, FileAttributes copyFilemap, ref FileAttributes xorFilemap, string chunkTempDirectory,
                                     int readWriteBuffer, Int64 xorUnitSize)
        {
            XorFileToChunks(localFile, chunkTempDirectory, ref xorFilemap, copyFilemap, xorUnitSize, readWriteBuffer);
        }

        public void CreateDefaultXorFilemap(FileAttributes localFile, ref FileAttributes xorFilemap, Int64 chunkSize)
        {
            xorFilemap.CopyFrom(localFile);
            SplitFileToChunks(ref xorFilemap, chunkSize, true);
        }

        // слить локальный файл с чанками, предназначенными для копирования, образовав
        // копию локального файла с новыми чанками
        public void MergeChunks(FileAttributes localFile, FileAttributes copyFilemap, ref FileAttributes mergedFile,
                                string chunkTempDirectory,
                                int readWriteBuffer)
        {
            if (copyFilemap.chunks.Count == 0)
                throw new Exception("Пустая карта копирования!");

            // проверка наличия всех необходимых чанков
            if (!IsAllFilesExists(chunkTempDirectory, copyFilemap.chunks))
                throw new Exception("Не все чанки были обнаружены!");

            // если локальный файл не существует -
            // просто соединяем все чанки из папки чанков в соответствии
            // с информацией из copyFilemap
            if (!File.Exists(localFile.Filename))
            {
                Int64 totalBytesRead = 0;
                for (int i = 0; i < copyFilemap.chunks.Count; i++)
                {
                    Int64 bytesCopied = PasteUsingBuffer(Path.Combine(chunkTempDirectory, copyFilemap.chunks[i].Id.ToString(CultureInfo.InvariantCulture)), 0,
                                     localFile.Filename, totalBytesRead, copyFilemap.chunks[i].Length, readWriteBuffer);
                    totalBytesRead += bytesCopied;
                }
                return;
            }

            // targetFile - либо mergedFile.Filename, если размеры
            // исходного и конечного файлов отличаются, либо
            // localFile.Filename, если размер остался прежним
            if (copyFilemap.FileSize < localFile.FileSize)
            {
                CopyUsingBuffer(localFile.Filename, 0, mergedFile.Filename, 0, copyFilemap.FileSize, readWriteBuffer);
            }
                    
            // добавить новые чанки
            for (int i = 0; i < copyFilemap.chunks.Count; i++)
            {
                PasteUsingBuffer(Path.Combine(chunkTempDirectory, copyFilemap.chunks[i].Id.ToString()), 0, localFile.Filename,
                                 copyFilemap.chunks[i].Start, copyFilemap.chunks[i].Length, readWriteBuffer);
            }
            //targetFile.Seek(copyFilemap.chunks[i].Start, SeekOrigin.Begin);
                //using (
                //    FileStream chunkFileStream =
                //        File.OpenRead(Path.Combine(chunkTempDirectory, copyFilemap.chunks[i].Id.ToString())))
                //{
                //    int totalBytesRead2 = 0;
                //    while (totalBytesRead2 < copyFilemap.chunks[i].Length)
                //    {
                //        Int32 bytesToRead2 = totalBytesRead2 + buffer.Length > copyFilemap.chunks[i].Length
                //                                 ? (Int32) (copyFilemap.chunks[i].Length - totalBytesRead2)
                //                                 : buffer.Length;

                //        int bytesRead2 = chunkFileStream.Read(buffer, 0, bytesToRead2);
                //        localFileStream.Write(buffer, 0, bytesRead2);
                //        totalBytesRead2 += bytesRead2;
                //    }
                //}


            //mergedFile.FileSize = localFileStream.Length;

            //xorFilemap.CopyFrom(localFile);
            //xorFilemap.FileSize = mergedFile.FileSize;
            //xorFilemap.Filename = mergedFile.Filename;

            //// обновить карту копирования
            //for (int i = 0; i < mergedFile.chunks.Count; i++)
            //{
            //    for (int j = 0; j < copyFilemap.chunks.Count; j++)
            //    {
            //        if (localFile.chunks[i].Start == copyFilemap.chunks[j].Start &&
            //            localFile.chunks[i].End == copyFilemap.chunks[j].End &&
            //            localFile.chunks[i].Length == copyFilemap.chunks[j].Length)
            //        {
            //            using (
            //                FileStream chunkTempFilestream =
            //                    File.OpenRead(Path.Combine(chunkTempDirectory, copyFilemap.chunks[i].Id.ToString())))
            //            {
            //                Chunk tempChunk = localFile.chunks[i];
            //                tempChunk.Type = XorChunks(localFile.chunks[i], copyFilemap.chunks[j], localFileStream,
            //                                           chunkTempFilestream, xorUnitSize, false);
            //                xorFilemap.chunks[i] = tempChunk;
            //            }
            //            break;
            //        }
            //    }
            //}

            //// все области, которые вне CopyFilemap пометить как статичные
            //for (int i = 0; i < localFile.chunks.Count; i++)
            //{
            //    if (mergedFile.chunks[i].Type == null)
            //    {
            //        Chunk tempChunk = localFile.chunks[i];
            //        tempChunk.Type = ChunkType.Static;
            //        tempChunk.Hash = GenerateChunkHash(localFile, tempChunk, true);
            //        xorFilemap.chunks[i] = tempChunk;
            //    }
            //}
        }

        public bool IsFileExists(FileAttributes file)
        {
            if (File.Exists(file.Filename))
                return true;
            else return false;
        }
    }
}
