﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilemapBackupSystem.Classes
{
    class Controller
    {
        private DataModelEditables dataModelEditables;

        public Controller()
        {
            this.dataModelEditables = new DataModelEditables();
        }

        // 1. на сервер отправляем карту копирования. 
        //      *если она пустая - делим весь файл на чанки, формируем из этого 
        // copyFilemap(без хэшей), передаем
        // карту и все чанки клиенту
        //      *если не пустая - считываем ее, делим локальный файл на части
        // среди карты ксор ищем схожие области. если схожая область из xorFilemap:
        //          * статичная - читаем эту область из файла, считаем ее хэш, сравниваем с
        //          хэшем из xorFilemap. если не совпали - добавляем в copyFilemap
        //          * иные виды совпавших областей добавляем в copyFilemap
        // если схожей области не нашлось - добавляем ее в copyFilemap как Extra
        // 2. сохраняем xorFilemap
        // 3. извлечь области, вошедшие в copyFilemap, в ChunksTempDir
        public void ServerWork()
        {
            dataModelEditables.server.WaitClient();
            dataModelEditables.server.SetPath(Path.Combine(dataModelEditables.rootDir, dataModelEditables.filesDirName));
            dataModelEditables.FileWorker.SplitFileToChunks(ref dataModelEditables.LocalFile, DataModelConstants.chunkSize, false);

            // принять xorFilemap от клиента
            dataModelEditables.server.ReceiveFile();
            LoadFilemap(FilemapTypes.xorFilemap);

            // составить карту копирования
            dataModelEditables.FileWorker.GenerateCopyFilemap(dataModelEditables.LocalFile, dataModelEditables.XorFilemap, ref dataModelEditables.CopyFilemap);
            
            SaveFilemap(FilemapTypes.copyFilemap);

            // отослать карту копирования клиенту
            dataModelEditables.server.WaitClient();
            dataModelEditables.server.SendFile(new FileInfo(dataModelEditables.CopyFilemap.FilemapName));

            // извлечь измененные чанки
            dataModelEditables.FileWorker.ExtractChunks(dataModelEditables.LocalFile, dataModelEditables.CopyFilemap,
                                                        dataModelEditables.ChunksTempDirectoryName,
                                                        dataModelEditables.ReadWriteBuffer);

            // отправить измененные чанки клиенту
            for (int i = 0; i < dataModelEditables.CopyFilemap.chunks.Count; i++)
            {
                dataModelEditables.server.WaitClient();
                dataModelEditables.server.SendFile(new FileInfo(Path.Combine(dataModelEditables.ChunksTempDirectoryName, dataModelEditables.CopyFilemap.chunks[i].Id.ToString())));
            }
        }

        public void SaveFilemap(string filemapType)
        {
            switch (filemapType)
            {
                case FilemapTypes.copyFilemap:
                    dataModelEditables.FilemapWorker.SaveFilemap(dataModelEditables.CopyFilemap);
                    break;
                case FilemapTypes.xorFilemap:
                    dataModelEditables.FilemapWorker.SaveFilemap(dataModelEditables.XorFilemap);
                    break;
            }
        }

        public void LoadFilemap(string filemapType)
        {
            switch (filemapType)
            {
                case FilemapTypes.copyFilemap:
                    dataModelEditables.FilemapWorker.LoadFilemap(ref dataModelEditables.CopyFilemap);
                    break;
                case FilemapTypes.xorFilemap:
                    dataModelEditables.FilemapWorker.LoadFilemap(ref dataModelEditables.XorFilemap);
                    break;
            }
        }

        //// +---------------------------------------------------+
        //#region Split

        //// разбить локальный хранимый файл на чанки
        //public void SplitLocalFile()
        //{
        //    dataModelEditables.FileWorker.SplitFileToChunks(ref dataModelEditables.LocalFile, DataModelConstants.chunkSize);
        //}

        //// разбить принятый файл на чанки
        //public void SplitReceivedFile()
        //{
        //    dataModelEditables.FileWorker.SplitFileToChunks(ref dataModelEditables.ReceivedFile, DataModelConstants.chunkSize);
        //}

        //#endregion Split
        //// +---------------------------------------------------+

        //// +---------------------------------------------------+
        //#region Xor

        //// сравнить принятый и локальный файлы операцией xor
        //// и сформировать карту чанков xor-filemap без хэшей
        //// на основе сравнения
        //public void XorLocalAndReceivedFiles()
        //{
        //    dataModelEditables.FileWorker.XorFiles(dataModelEditables.LocalFile,
        //                                           dataModelEditables.ReceivedFile,
        //                                           ref dataModelEditables.XorFilemap,
        //                                           DataModelConstants.xorUnitSize);
        //}

        //// добавить хэши к xor-карте
        //public void GenerateXorFilemapHashes()
        //{
        //    dataModelEditables.FileWorker.GenerateFilesChunksHash(ref dataModelEditables.XorFilemap, dataModelEditables.LocalFile);
        //}
        //#endregion Xor
        //// +---------------------------------------------------+
        //#region Split/Merge
        //public void ExtractChunks()
        //{
        //    dataModelEditables.FileWorker.ExtractChunks(dataModelEditables.ReceivedFile, dataModelEditables.CopyFilemap,
        //                                                dataModelEditables.ChunksTempDirectory,
        //                                                dataModelEditables.ReadWriteBuffer);
        //}

        //public void MergeChunks()
        //{
        //    dataModelEditables.FileWorker.MergeChunks(dataModelEditables.LocalFile, dataModelEditables.CopyFilemap,
        //                                              ref dataModelEditables.MergedFile,
        //                                              dataModelEditables.ChunksTempDirectory,
        //                                              dataModelEditables.ReadWriteBuffer, DataModelConstants.chunkSize, DataModelConstants.xorUnitSize);
        //}
        //#endregion Split/Merge
        //// +---------------------------------------------------+

        //// сформировать карту копирования на основе карты операции xor
        //// и локально хранимого файла
        //public void GenerateCopyFilemap()
        //{
        //    dataModelEditables.FileWorker.GenerateCopyFilemap(dataModelEditables.LocalFile,
        //                                                      dataModelEditables.XorFilemap,
        //                                                      ref dataModelEditables.CopyFilemap,
        //                                                      DataModelConstants.chunkSize);
        //}

    }
}
